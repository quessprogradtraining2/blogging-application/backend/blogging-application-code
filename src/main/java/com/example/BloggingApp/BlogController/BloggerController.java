package com.example.BloggingApp.BlogController;

import com.example.BloggingApp.BlogModel.Blogger;
import com.example.BloggingApp.BlogService.BloggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BloggerController {

    @Autowired
    BloggerService bloggerServiceObject;


    @PostMapping("/addblog")
    public String addBlog(@RequestBody Blogger bloggerObject){
        String stringObject=bloggerServiceObject.addBlog(bloggerObject);
        return stringObject;
    }

    @PutMapping("/updateblog/{blogId}")
    public void updateBlog(@RequestBody Blogger bloggerobject, @PathVariable int blogId){
        bloggerServiceObject.updateBlog(bloggerobject,blogId);
    }

    @DeleteMapping("/deleteBlog/{blogId}")
    public void deleteBlog(@PathVariable int blogId){
        bloggerServiceObject.deleteBlog(blogId);
    }

    @GetMapping("/displayBlog")
    public List<Blogger> displayBlog(){
        return bloggerServiceObject.displayBlog();
    }
}
