package com.example.BloggingApp.BlogService;

import com.example.BloggingApp.BlogModel.Blogger;
import com.example.BloggingApp.BlogRepository.BloggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BloggerService {

    @Autowired
    BloggerRepository bloggerRepositoryObject;
    public String addBlog(Blogger bloggerObject) {
        bloggerRepositoryObject.save(bloggerObject);
        return "Blog is added successfully";
    }

    public void updateBlog(Blogger bloggerobject, int blogId) {
        Blogger fetchedBlog=bloggerRepositoryObject.findById(blogId).get();
        if(fetchedBlog!=null){
            bloggerRepositoryObject.delete(fetchedBlog);
            bloggerRepositoryObject.save(bloggerobject);

        }
    }

    public void deleteBlog(int blogId) {
        bloggerRepositoryObject.deleteById(blogId);
    }

    public List<Blogger> displayBlog() {
        return bloggerRepositoryObject.findAll();
    }
}
