package com.example.BloggingApp.BlogService;

import com.example.BloggingApp.BlogModel.Users;
import com.example.BloggingApp.BlogRepository.UserRepository;
import com.example.BloggingApp.BlogSecurity.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Users user = userRepository.findByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException("User not found!..");
        }
        return new MyUserDetails(user);
    }
}
