package com.example.BloggingApp.BlogRepository;

import com.example.BloggingApp.BlogModel.Blogger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BloggerRepository extends JpaRepository<Blogger,Integer> {
}
