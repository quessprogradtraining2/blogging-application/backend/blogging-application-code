package com.example.BloggingApp.BlogRepository;

import com.example.BloggingApp.BlogModel.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users,Integer> {

    Users findByUserName(String userName);
}
